import pandas as pd
from tqdm.notebook import tqdm
import snscrape.modules.twitter as sntwitter
import json

scraper = sntwitter.TwitterHashtagScraper("#хтивийпонеділок")

tweets = []
num_of_tweets = 123
for i, tweet in enumerate(scraper.get_items()):
    if not tweet.media:
        continue
    for ph in tweet.media:
        if isinstance(ph, sntwitter.Photo):

            data = [
                tweet.user.username,
                ph.fullUrl
            ]
            tweets.append(data)

    if i > num_of_tweets:
        break

""" file_path = './photos.json'
with open(file_path, 'w') as f:
    json.dump(tweets, f) """

""" tweet_df = pd.DataFrame(tweets, columns=['username', 'fullUrl'])
tweet_df.to_csv('./photos.csv', index=False) """
